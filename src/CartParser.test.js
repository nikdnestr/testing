import CartParser from './CartParser';

let parser, parseLine, readFile, validate, calcTotal, parse;

const dumData = 
`Product name,Price,Quantity
Useless, 9.10, 15
VeryUseless, 99.10, 4
UberUseless, 999.10, 1`;

const dumItems = dumData.split('\n');

beforeEach(() => {
	parser = new CartParser();
  parseLine = parser.parseLine.bind(parser);
  readFile = parser.readFile.bind(parser);
  validate = parser.validate.bind(parser);
	calcTotal = parser.calcTotal.bind(parser);
	parse = parser.parse.bind(parser);
});

describe('CartParser - unit tests', () => {
	it('should return correct name', () => {
		const data = parser.parseLine(dumItems[1]);
		expect(data.name).toBe('Useless')
	});
	it('should return correct price', () => {
		const data = parser.parseLine(dumItems[1]);
		expect(data.price).toBe(9.1)
	});
	it('should return correct quantity', () => {
		const data = parser.parseLine(dumItems[1]);
		expect(data.quantity).toBe(15)
	});
	it('should return correct total price', () => {
		const data = [];
		for (let i = 1; i < dumItems.length; i++) {
			const element = dumItems[i];
			data.push(parser.parseLine(element))
		}
		expect(calcTotal(data)).toEqual(1532)
	})
	it('should return succesfull validation', () => {
		expect(validate(dumData)).toEqual([]);
	})
	it('should return ENOENT', () => {
		try {
			const path = 'samples/car.csv'
			readFile(path)
		} catch (error) {
			expect(error.code).toBe('ENOENT')
		}
	})
	it('should return JSON', () => {
		const type = typeof parse('samples/cart.csv')
		expect(type).toEqual('object');
	})
});

describe('CartParser - integration test', () => {
	it('should contain all properties', () => {
		const data = parse('samples/cart.csv');
		expect(data.items[0]).toHaveProperty('name')
		expect(data.items[0]).toHaveProperty('price')
		expect(data.items[0]).toHaveProperty('quantity')
	})
});